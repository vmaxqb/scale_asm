import React from 'react'
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

export default class EditProduct extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            product: this.props.product,
            dialogShowCount: 0
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (state.dialogShowCount != props.dialogShowCount) {
            return {
                product: props.product,
                dialogShowCount: props.dialogShowCount
            }
        }

        return null
    }

    nameChangeHandle = (e) => {
        let newProduct = {...this.state.product}
        newProduct.name = e.target.value
        this.setState({
            product: newProduct
        })
    }

    descriptionChangeHandle = (e) => {
        let newProduct = {...this.state.product}
        newProduct.description = e.target.value
        this.setState({
            product: newProduct
        })
    }

    priceChangeHandle = (e) => {
        let newProduct = {...this.state.product}
        newProduct.price = e.target.value
        this.setState({
            product: newProduct
        })
    }

    render() {
        return (
            <Modal show={this.props.visible} onHide={this.props.hideHandle}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {this.state.product.id != 0 && <span>Edit the product</span>}
                        {this.state.product.id == 0 && <span>New product</span>}
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form.Group controlId="name">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter name" required={true} onChange={this.nameChangeHandle} value={this.state.product.name} />
                    </Form.Group>

                    <Form.Group controlId="description">
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" placeholder="Enter description" required={true} onChange={this.descriptionChangeHandle} value={this.state.product.description} />
                    </Form.Group>

                    <Form.Group controlId="price">
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="text" placeholder="Enter price" required={true} onChange={this.priceChangeHandle} value={this.state.product.price} />
                    </Form.Group>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary">Close</Button>
                    <Button variant="primary" onClick={() => this.props.onDialogSubmit(this.state.product)}>Save changes</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}
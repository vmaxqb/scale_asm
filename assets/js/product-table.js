import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrashAlt, faSortUp, faSortDown } from '@fortawesome/free-solid-svg-icons'

export default function ProductTable(props) {
    const products = props.products.map((val) => {
        return (
            <tr key={val.id}>
                <td>
                    <a href="#" onClick={(e) => props.editFunc(val.id)}><FontAwesomeIcon icon={faEdit} /></a>
                    &nbsp;
                    <a href="#" onClick={(e) => props.deleteFunc(val.id)}><FontAwesomeIcon icon={faTrashAlt} /></a>
                </td>
                <td>{val.id}</td>
                <td>{val.name}</td>
                <td>{val.description}</td>
                <td>{val.price}</td>
            </tr>
        )
    })

    return (
        <table border="1">
            <thead>
                <tr>
                    <th>Actions</th>
                    <th className="text-nowrap">
                        <a href="#" onClick={(e) => props.changeSort('id')}>
                            Id
                            {props.sort == 'id' && <FontAwesomeIcon icon={faSortUp} />}
                            {props.sort == '-id' && <FontAwesomeIcon icon={faSortDown} />}
                        </a>
                    </th>
                    <th className="text-nowrap">
                        <a href="#" onClick={(e) => props.changeSort('name')}>
                            Name
                            {props.sort == 'name' && <FontAwesomeIcon icon={faSortUp} />}
                            {props.sort == '-name' && <FontAwesomeIcon icon={faSortDown} />}
                        </a>
                    </th>
                    <th className="text-nowrap">
                        <a href="#" onClick={(e) => props.changeSort('description')}>
                            Description
                            {props.sort == 'description' && <FontAwesomeIcon icon={faSortUp} />}
                            {props.sort == '-description' && <FontAwesomeIcon icon={faSortDown} />}
                        </a>
                    </th>
                    <th className="text-nowrap">
                        <a href="#" onClick={(e) => props.changeSort('price')}>
                            Price
                            {props.sort == 'price' && <FontAwesomeIcon icon={faSortUp} />}
                            {props.sort == '-price' && <FontAwesomeIcon icon={faSortDown} />}
                        </a>
                    </th>
                </tr>
            </thead>
            <tbody>
                {products}
            </tbody>
        </table>
    )
}
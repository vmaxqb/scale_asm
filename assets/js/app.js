import React from 'react'
import ReactDOM from 'react-dom'
import Axios from 'axios'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons'
import Button from 'react-bootstrap/Button'
import ProductTable from './product-table'
import EditProduct from './edit-product'

import 'bootstrap/dist/css/bootstrap.min.css'
import '../css/app.css';

const emptyProduct = {
    id: 0,
    name: '',
    description: '',
    price: 0
}

class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            products: [],
            limit: 25,
            page: 0,
            sort: 'id',
            editDialogVisible: false,
            productForEditing: {},
            deleteDialogVisible: false,
            dialogShowCount: 0
        }
    }
    
    componentDidMount = () => {
        this.getProducts()
    }

    changeSort = (sortCol) => {
        let newSort
        if (sortCol == this.state.sort)
            newSort = '-' + sortCol
        else
            newSort = sortCol
        
        this.setState({
            sort: newSort
        }, this.getProducts)
    }

    changePage = (inc) => {
        let newPage = this.state.page + inc;
        if (newPage < 0)
            newPage = 0
        
        this.setState({
            page: newPage
        }, this.getProducts)
    }

    changePageSize = (e) => {
        this.setState({
            limit: e.target.value
        }, this.getProducts)
    }

    getProducts = () => {
        Axios.get('/products', {
            params: {
                limit: this.state.limit,
                page: this.state.page,
                sort: this.state.sort
            }
        }).then((response) => {
            this.setState({
                products: response.data
            })
        })
    }

    editViewProduct = (id) => {
        Axios({
            method: 'get',
            url: '/products/' + id
        }).then((response) => {
            this.setState({
                productForEditing: response.data,
                editDialogVisible: true,
                dialogShowCount: this.state.dialogShowCount + 1
            })
        })
    }

    editDialogHide = () => {
        this.setState({
            editDialogVisible: false
        })
    }

    editDialogSubmit = (product) => {
        this.editDialogHide()
        if (product.id != 0) {
            Axios({
                method: 'put',
                url: '/products/' + this.state.productForEditing.id,
                data: product
            }).then(this.getProducts)
        }
        else {
            Axios({
                method: 'post',
                url: '/products',
                data: product
            }).then(this.getProducts)
        }
    }

    newViewProduct = () => {
        this.setState({
            productForEditing: emptyProduct,
            editDialogVisible: true,
            dialogShowCount: this.state.dialogShowCount + 1
        })
    }

    deleteProduct = (id) => {
        if (!confirm('Really delete?'))
            return
        
        Axios({
            method: 'delete',
            url: '/products/' + id
        }).then(this.getProducts)
    }

    render() {
        return (
            <div>
                <div className="w-100 d-flex justify-content-around py-3">
                    <div>
                        <a href="#" onClick={(e) => this.changePage(-1)}>
                            <FontAwesomeIcon icon={faChevronLeft} />
                        </a>
                        Pages
                        <a href="#" onClick={(e) => this.changePage(1)}>
                            <FontAwesomeIcon icon={faChevronRight} />
                        </a>
                    </div>
                    <div>
                        Page size:
                        <select className="form-control d-inline w-auto" onChange={this.changePageSize} defaultValue={this.state.limit}>
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                        </select>
                    </div>
                    <div>
                        <Button variant="primary" onClick={this.newViewProduct}>New product</Button>
                    </div>
                </div>
                <ProductTable
                    products={this.state.products}
                    editFunc={this.editViewProduct}
                    deleteFunc={this.deleteProduct}
                    changeSort={this.changeSort}
                    sort={this.state.sort}
                />

                <EditProduct
                    visible={this.state.editDialogVisible}
                    hideHandle={this.editDialogHide}
                    product={this.state.productForEditing}
                    onDialogSubmit={this.editDialogSubmit}
                    dialogShowCount={this.state.dialogShowCount}
                />
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'))

<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        for ($i = 0; $i < 50; $i++) {
            $product = new Product();
            $product->setName($faker->name());
            $product->setDescription($faker->realText());
            $product->setPrice($faker->randomFloat(2));
            $manager->persist($product);
        }
        $manager->flush();
    }
}

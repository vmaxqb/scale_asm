<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/products", name="api_get_products", methods={"GET"})
     */
    public function getProducts(Request $request)
    {
        $page = $request->get('page', 0);
        $limit = $request->get('limit', 25);
        $sort = $request->get('sort', 'id');
        $sortSql = [
            $sort => 'ASC',
        ];
        if (strpos($sort, '-') === 0) {
            $sortSql = [
                substr($sort, 1) => 'DESC',
            ];
        }

        $products = $this->getDoctrine()->getRepository(Product::class)->findBy([], $sortSql, $limit, $page * $limit);
        $productsArray = array_map(function ($product) {
            return $product->toArray();
        }, $products);


        return $this->json($productsArray);
    }

    /**
     * @Route("/products", name="api_post_product", methods={"POST"})
     */
    public function postProduct(Request $request)
    {
        if (strpos($request->headers->get('Content-Type'), 'application/json') !== 0)
            throw new BadRequestHttpException();
        
        $json = $request->getContent();
        $data = json_decode($json);
        if (empty($data))
            throw new BadRequestHttpException();

        $product = new Product();
        $product
            ->setName($data->name)
            ->setDescription($data->description)
            ->setPrice($data->price)
        ;

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        return $this->json($product->toArray());
    }

    /**
     * @Route("/products/{id}", name="api_get_product", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function getProduct(int $id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        if (!$product)
            throw $this->createNotFoundException();

        return $this->json($product);
    }

    /**
     * @Route("/products/{id}", name="api_put_product", methods={"PUT"}, requirements={"id"="\d+"})
     */
    public function putProduct(Request $request, int $id)
    {
        if (strpos($request->headers->get('Content-Type'), 'application/json') !== 0)
            throw new BadRequestHttpException();
        
        $json = $request->getContent();
        $data = json_decode($json);
        if (empty($data))
            throw new BadRequestHttpException();

        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        if (!$product)
            throw $this->createNotFoundException();

        if (isset($data->name))
            $product->setName($data->name);
        if (isset($data->description))
            $product->setDescription($data->description);
        if (isset($data->price))
            $product->setPrice($data->price);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        return $this->json($product->toArray());
    }

    /**
     * @Route("/products/{id}", name="api_delete_product", methods={"DELETE"}, requirements={"id"="\d+"})
     */
    public function deleteProduct(int $id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        if (!$product)
            throw $this->createNotFoundException();

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($product);
        $entityManager->flush();

        return $this->json([]);
    }
}

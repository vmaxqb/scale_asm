# What is it about?

This is just another test of skills.

We have a REST API, based on Symfony

We have a frontend, based on React and Symfony's Twig.

That's it. 

## Dev deployment

1. Clone the repository `git clone https://gitlab.com/vmaxqb/scale_asm.git`

2. Make a MySQL database and a user

3. Create a file `.env.dev.local` and put a line in it. Change this line with your database connection attributes.

```
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
```

4. Install all PHP dependences: `composer install`

5. Install all JS dependences: `npm install`

6. Create a database structure: `bin/console doctrine:migrations:migrate`

7. You can compile the JS-files with `yarn encore dev --watch`

8. Start the web-server with `symfony serve`. [Symfony CLI](https://symfony.com/download) should be installed.

9. Open [http://127.0.0.1:8000](http://127.0.0.1:8000) in you browser.

10. Enjoy

## Production deployment

Follow the [official deployment recommendations](https://symfony.com/doc/current/deployment.html)
